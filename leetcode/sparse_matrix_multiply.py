#Given two sparse matrices A and B, return the result of AB
#assumes correct matrix dimensions
def multiply(A, B):
    output = [[0 for _ in range(len(B[0]))] for _ in range(len(A))]

    for i, row in enumerate(A):
        for k, row_val in enumerate(row):
            if row_val != 0:
                for j, col_val in enumerate(B[k]):
                    output[i][j] += row_val * col_val

    return output

print multiply([[2, 4], [3, 4]], [[2, 8], [1, 7]])
