#Suppose you have n versions [1, 2, ..., n] and you want to find out the first bad one,
#which causes all the following ones to be bad.
#You are given an API bool isBadVersion(version) which will return whether version is bad.
#Implement a function to find the first bad version. You should minimize the number of calls to the API
def firstBadVersion(n):
    def binary_search(start, end):
        mid = (start + end) / 2

        if isBadVersion(mid):
            if not isBadVersion(mid-1):
                return mid
            else:
                return binary_search(start, mid-1)
        else:
            return binary_search(mid+1, end)

    return binary_search(0, n)

#this one doesn't run outside of leetcode as we don't have access to the isBadVersion API
