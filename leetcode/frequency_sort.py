#sort letters in a string by frequency of occurence
def frequencySort(s):
    """
    :type s: str
    :rtype: str
    """
    letters = {}
    output = ""

    for char in s:
        if char not in letters:
            letters[char] = 1
        else:
            letters[char] += 1

    while letters != {}:
        letter = max(letters, key=letters.get)
        numberOfOccurrences = letters[letter]

        output += letter * numberOfOccurrences
        letters.pop(letter, None)

    return output

print frequencySort("All I want for Christmas is my two front teeth!")
