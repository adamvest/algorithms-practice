#Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0
#Find all unique triplets in the array which gives the sum of zero
def threeSum(nums):
    output = []
    solutions = {}
    nums.sort()

    for i in range(len(nums)):
        hash = {}
        target, j = 0 - nums[i], i + 1

        while j < len(nums):
            if nums[j] in hash:
                num_str = str(nums[i]) + str(nums[hash[nums[j]]]) + str(nums[j])

                if num_str not in solutions:
                    output.append([nums[i], nums[hash[nums[j]]], nums[j]])
                    solutions[num_str] = True

            hash[target - nums[j]] = j
            j += 1

    return output

print threeSum([-1, 0, 1, 2, -1, -4])
