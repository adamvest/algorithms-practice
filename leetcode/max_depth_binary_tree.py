#Given a binary tree, find its maximum depth
def maxDepth(root):
    depth, left_depth, right_depth = 0, 0, 0

    if root != None:
        if root.l != None:
            left_depth = maxDepth(root.l)
        if root.r != None:
            right_depth = maxDepth(root.r)

        depth = max(left_depth, right_depth) + 1

    return depth

class Node(object):
    def __init__(self, x):
        self.v = x
        self.l = None
        self.r = None

class Tree:
    def __init__(self):
        self.root = None

    def getRoot(self):
        return self.root

    def add(self, val):
        if(self.root == None):
            self.root = Node(val)
        else:
            self._add(val, self.root)

    def _add(self, val, node):
        if(val < node.v):
            if(node.l != None):
                self._add(val, node.l)
            else:
                node.l = Node(val)
        else:
            if(node.r != None):
                self._add(val, node.r)
            else:
                node.r = Node(val)

    def printTree(self):
        if(self.root != None):
            self._printTree(self.root)

    def _printTree(self, node):
        if(node != None):
            self._printTree(node.l)
            print str(node.v) + ' '
            self._printTree(node.r)

tree = Tree()
tree.add(3)
tree.add(4)
tree.add(0)
tree.add(8)
tree.add(2)
print maxDepth(tree.root)
