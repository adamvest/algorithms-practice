#Given an array nums and a target value k, find the maximum length of a subarray that sums to k
#If there isn't one, return 0 instead.
def maxSubArrayLen(nums, k):
    sums = {}
    current_sum, length = 0, 0

    for i in range(len(nums)):
        #running sum
        current_sum += nums[i]

        #if current sum is equal to k, update length (Note: i+1 will always be longer than any stored sequence, so just assign it)
        if current_sum == k:
            length = i + 1
        #else if current sum - k is in sums (i.e., sums[someKey] + k = currentSum), then length = i - someKey
        #as i is the index of the current running sum and someKey is the index of some earlier running sum
        elif current_sum - k in sums:
            length = max(length, i - sums[current_sum - k])

        #store each distinct sum in the sums dict as sum:index where sum is the sum of all values from 0-index inclusive
        if current_sum not in sums:
            sums[current_sum] = i

    return length

print maxSubArrayLen([8, 6, 7, 5, 3, 0, 9], 8)
