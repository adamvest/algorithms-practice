#Given two binary strings, return their sum (also a binary string)
def addBinary(a, b):
    a, b = [ord(char) - 48 for char in a[::-1]], [ord(char) - 48 for char in b[::-1]]
    carry, result_length = 0, max(len(a), len(b))
    result = [""] * result_length

    for i in range(result_length):
        val = carry

        if i < len(a):
            val += a[i]
        if i < len(b):
            val += b[i]

        carry, bit_val = divmod(val, 2)
        result[i] = chr(bit_val + 48)

    if carry == 1:
        result.append("1")

    return ''.join(result[::-1])

print addBinary("110011", "1000100")
