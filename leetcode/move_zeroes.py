#Given an array nums, write a function to move all 0's to the end of it
#while maintaining the relative order of the non-zero elements.
def moveZeroes(nums):
    i, count = 0, 0

    while i < len(nums):
        if nums[i] == 0:
            del nums[i]
            count += 1
        else:
            i += 1

    for j in range(count):
        nums.append(0)

    return nums

print moveZeroes([0, 1, 0, 3, 12])
