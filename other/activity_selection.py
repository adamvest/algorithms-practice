#find the largest number of non-overlapping activities
def activity_selection(activities):
    c = [[1 for _ in range(len(activities))] for _ in range(len(activities))]
    max_number_of_activities = 0
    
    for i in range(len(activities)):
        for j in range(i + 1, len(activities)):
            for k in range(i + 1, j):
                if activities[i][1] <= activities[k][0] and activities[k][1] <= activities[j][0]:
                    if c[i][j] < c[i][k] + c[k][j]:
                        c[i][j] = c[i][k] + c[k][j]
    
    for x in range(len(c)):
        for y in range(len(c[0])):
            if c[x][y] > max_number_of_activities:
                max_number_of_activities = c[x][y]
    
    return max_number_of_activities + 1

activities = [[0, 7], [2, 9], [5, 10], [11, 12], [13, 17], [20, 21]]
print activities
print activity_selection(activities)
