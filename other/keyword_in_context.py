#get the context (ie, given number of words around) a given keyword in a file
def getKeywordContexts(filename, keyword, contextSize):
    output = []

    with open(filename) as file:
        for line in file:
            tokens = line.split()
            
            if keyword in tokens:
                index = tokens.index(keyword)
                start, end = 0, len(tokens) - 1
                
                if index >= contextSize:
                    start = index - contextSize
                if len(tokens) - contextSize - 1 > index:
                    end = index + contextSize
                
                output.append(" ".join(tokens[start : end+1]))
    
    return output
    
print getKeywordContexts("sampleKeywordContextData.txt", "hi", 5)
