#find the length of the longest increasing subsequence
def longest_increasing_subsequece(arr):
    lengths = [1] * (len(arr))
    max_length = 0
    
    for i in range(len(arr)):
        for j in range(i):
            if arr[i] > arr[j]:
                lengths[i] = lengths[j] + 1
                
    for k in range(len(lengths)):
        if lengths[k] > max_length:
            max_length = lengths[k]
            
    return max_length

arr = [1, 3, 7, 2, 4, 9, 5, 6, 10, 11]

print longest_increasing_subsequece(arr)
