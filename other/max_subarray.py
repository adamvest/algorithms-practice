#find the index nounds and value of the max subarray sequence
def get_max_subarray(arr):
    current_max, current_leftbound, current_rightbound = 0, 0, 0
    subarray_max, subarray_leftbound, subarray_rightbound = 0, 0, 0
    
    for i in range(len(arr)):
        current_max += arr[i]
        current_rightbound = i

        if current_max <= 0:
            current_leftbound = i+1
            current_max = 0
        elif current_max > subarray_max:
            subarray_max = current_max
            subarray_leftbound = current_leftbound
            subarray_rightbound = current_rightbound
            
    return subarray_max, subarray_leftbound, subarray_rightbound

arr = [-2, 1, 6, 7, -3, 5, -8, 12, -7, -6, -22, 5, 30]
subarray_max, subarray_leftbound, subarray_rightbound = get_max_subarray(arr)

print "arr: ", arr
print "max: " + str(subarray_max)
print "left bound index: " + str(subarray_leftbound)
print "right bound index: " + str(subarray_rightbound)
