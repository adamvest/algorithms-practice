#find all possible products of a given list of primes
def products(primes):
    result = [1]
    for p in primes:
        result += [x * p for x in result]
    return result

print products([2, 3, 11, 22, 37, 89, 10, 11, 29])
