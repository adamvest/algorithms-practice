/*
* Program to "simulate" Non-preemptive and preemptive priority based CPU scheduling
* In this context, "simulate" refers to performing the selection algorithms and 
* Maintaining the PCBs of simulated processes as the OS would
* Note: Everything is in one file for class submission requirements
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct pcb
{
   int pid;
   int arrivalTime;
   int burstTime;
   int remainingBurstTime;
   int priority;
   struct pcb *prev;
   struct pcb *next;
} PCB;

typedef struct
{
   PCB *head;
   PCB *tail;
} List;

PCB * CreatePCB(int pid, int arrivalTime, int burstTime, int priority)
{
   PCB *block = (PCB *)malloc(sizeof(PCB));

   if(block == NULL)
   {
      fprintf(stderr, "%s\n", "Failed to create block");
      exit(-1);
   }

   block->pid = pid;
   block->arrivalTime = arrivalTime;
   block->burstTime = burstTime;
   block->remainingBurstTime = burstTime;
   block->priority = priority;
   block->prev = NULL;
   block->next = NULL;

   return block;
}

List * CreateList()
{
   List *list = (List *)malloc(sizeof(List));

   if(list == NULL)
   {
      fprintf(stderr, "%s\n", "Failed to create list");
      exit(-1);
   }

   list->head = NULL;
   list->tail = NULL;

   return list;
}

void DestroyList(List *list)
{
   PCB *ptr = list->head;
   PCB *temp;

   while(ptr != NULL)
   {
      temp = ptr;
      ptr = ptr->next;
      free(temp);
   }

   free(list);
}

void Append(List *list, int pid, int arrivalTime, int burstTime, int priority)
{
   PCB *newBlock = CreatePCB(pid, arrivalTime, burstTime, priority);

   if(list->tail == NULL)
   {
      list->head = newBlock;
      list->tail = newBlock;
   }
   else
   {
      list->tail->next = newBlock;
      newBlock->prev = list->tail;
      list->tail = newBlock;
   }
}

PCB * Find(List *list, int arrivalTime)
{
   PCB *block = list->head;

   while(block != NULL)
   {
      if(block->arrivalTime == arrivalTime)
      {
         return block;
      }

      block = block->next;
   }

   return NULL;
}

PCB * FindByPid(List *list, int pid)
{
   PCB *block = list->head;

   while(block != NULL)
   {
      if(block->pid == pid)
      {
         return block;
      }

      block = block->next;
   }

   return NULL;
}

void DeleteProcess(List *processList, List *readyQueue, int pid)
{
   PCB *listBlock = FindByPid(processList, pid);
   PCB *readyBlock = FindByPid(readyQueue, pid);

   if(processList->head == listBlock && processList->tail == listBlock)
   {
      processList->head = processList->tail = NULL;
   }
   else if(processList->head == listBlock)
   {
      processList->head = processList->head->next;
      processList->head->prev = NULL;
   }
   else if(processList->tail == listBlock)
   {
      processList->tail = processList->tail->prev;
      processList->tail->next = NULL;
   }
   else
   {
      listBlock->prev->next = listBlock->next;
      listBlock->next->prev = listBlock->prev;
   }

   if(readyQueue->head == readyBlock && readyQueue->tail == readyBlock)
   {
      readyQueue->head = readyQueue->tail = NULL;
   }
   else if(readyQueue->head == readyBlock)
   {
      readyQueue->head = readyQueue->head->next;
      readyQueue->head->prev = NULL;
   }
   else if(readyQueue->tail == readyBlock)
   {
      readyQueue->tail = readyQueue->tail->prev;
      readyQueue->tail->next = NULL;
   }
   else
   {
      readyBlock->prev->next = readyBlock->next;
      readyBlock->next->prev = readyBlock->prev;
   }

   free(listBlock);
   free(readyBlock);
}

void PrintList(List *list)
{
   PCB *block = list->head;

   while(block != NULL)
   {
      printf("%d %d %d %d\n", block->pid, block->arrivalTime, block->burstTime, block->priority);
      block = block->next;
   }
}

void ReadFileToList(List *list, char *filename, int limit)
{
   FILE *file;
   int pid = 0, arrivalTime = 0, burstTime = 0, priority = 0;
   file = fopen(filename, "r");

   if(limit != 0)
   {
      while(fscanf(file, "%d %d %d %d", &pid, &arrivalTime, &burstTime, &priority) == 4 && limit > 0)
      {
         Append(list, pid, arrivalTime, burstTime, priority);
         limit--;
      }
   }
   else
   {
      while(fscanf(file, "%d %d %d %d", &pid, &arrivalTime, &burstTime, &priority) == 4)
      {
         Append(list, pid, arrivalTime, burstTime, priority);
      }
   }

   fclose(file);
}

PCB * ChooseBlockToExecute(List *readyQueue)
{
   PCB *block = readyQueue->head, *blockToChoose = NULL;

   while(block != NULL)
   {
      if(blockToChoose == NULL || blockToChoose->priority > block->priority)
      {
         blockToChoose = block;
      }
      else if(blockToChoose->priority == block->priority
         && blockToChoose->arrivalTime > block->arrivalTime)
      {
         blockToChoose = block;
      }

      block = block->next;
   }

   return blockToChoose;
}

void WriteExecutionDataToFile(FILE *file, PCB *finishedBlock, int finishTime)
{
   fprintf(file, "%d %d %d %d\n", finishedBlock->pid, finishedBlock->arrivalTime, finishTime,
                           finishTime - (finishedBlock->arrivalTime + finishedBlock->burstTime));
}

PCB * ExecuteBlock(FILE *file, List *processList, List *readyQueue, PCB *blockToExecute, int t)
{
   blockToExecute->remainingBurstTime--;

   if(blockToExecute->remainingBurstTime == 0)
   {
      WriteExecutionDataToFile(file, blockToExecute, t+1);
      DeleteProcess(processList, readyQueue, blockToExecute->pid);
      return NULL;
   }

   return blockToExecute;
}

void NonPreemptiveSchedulingSimulation(char *filename, List *processList, List *readyQueue)
{
   int t = 0;
   PCB *incomingProcess = NULL, *blockToExecute = NULL;
   FILE *file = fopen(filename, "w");

   while(processList->head != NULL)
   {
      incomingProcess = Find(processList, t);

      if(incomingProcess != NULL)
      {
         Append(readyQueue, incomingProcess->pid, incomingProcess->arrivalTime,
                  incomingProcess->burstTime, incomingProcess->priority);
      }

      if(blockToExecute == NULL)
      {
         blockToExecute = ChooseBlockToExecute(readyQueue);
      }

      if(blockToExecute != NULL)
      {
         blockToExecute = ExecuteBlock(file, processList, readyQueue, blockToExecute, t);
      }

      t++;
   }

   fclose(file);
}

void PreemptiveSchedulingSimulation(char *filename, List *processList, List *readyQueue)
{
   int t = 0;
   PCB *incomingProcess = NULL, *blockToExecute = NULL;
   FILE *file = fopen(filename, "w");

   while(processList->head != NULL)
   {
      incomingProcess = Find(processList, t);

      if(incomingProcess != NULL)
      {
         Append(readyQueue, incomingProcess->pid, incomingProcess->arrivalTime,
                  incomingProcess->burstTime, incomingProcess->priority);
      }

      blockToExecute = ChooseBlockToExecute(readyQueue);

      if(blockToExecute != NULL)
      {
         blockToExecute = ExecuteBlock(file, processList, readyQueue, blockToExecute, t);
      }

      t++;
   }

   fclose(file);
}

int main(int argc, char *argv[])
{
   if(argc != 4 && argc != 5)
   {
      fprintf(stderr, "%s\n", "Incorrect number of arguments provided");
      exit(-1);
   }

   char *infile = argv[1];
   char *outfile = argv[2];
   char *algorithm = argv[3];
   int limit = (argc == 5) ? atoi(argv[4]) : 0;

   List *processList = CreateList();
   List *readyQueue = CreateList();

   ReadFileToList(processList, infile, limit);

   if(strcmp(algorithm, "NPP") == 0)
   {
      NonPreemptiveSchedulingSimulation(outfile, processList, readyQueue);
   }
   else if(strcmp(algorithm, "PP") == 0)
   {
      PreemptiveSchedulingSimulation(outfile, processList, readyQueue);
   }

   DestroyList(processList);
   DestroyList(readyQueue);
}
