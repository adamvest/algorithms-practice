#program to define a LSTM network to perform word-level text generation
import sys
import numpy
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils

# load ascii text and covert to lowercase
filename = "GrandInquisitor.txt"
raw_text = open(filename).read().lower()

# create mapping of unique words to integers, and a reverse mapping
all_words = raw_text.split()
words = sorted(list(set(all_words)))
word_to_int = dict((w, i) for i, w in enumerate(words))
int_to_word = dict((i, w) for i, w in enumerate(words))

# summarize the loaded data
n_words = len(all_words)
n_vocab = len(words)
print "Number of Words: ", n_words
print "Vocab Size: ", n_vocab

# prepare the dataset of input to output pairs encoded as integers
seq_length = 20
dataX = []
dataY = []

for i in range(0, n_words - seq_length):
	seq_in = all_words[i:i + seq_length]
	seq_out = all_words[i + seq_length]
	dataX.append([word_to_int[word] for word in seq_in])
	dataY.append(word_to_int[seq_out])

n_patterns = len(dataX)
print "Total Patterns: ", n_patterns

# reshape X to be [samples, time steps, features]
X = numpy.reshape(dataX, (n_patterns, seq_length, 1))
# normalize
X = X / float(n_vocab)
# one hot encode the output variable
Y = np_utils.to_categorical(dataY)

# define the LSTM model
model = Sequential()
model.add(LSTM(256, input_shape = (X.shape[1], X.shape[2]), return_sequences = True))
model.add(Dropout(0.2))
model.add(LSTM(256))
model.add(Dropout(0.2))
model.add(Dense(Y.shape[1], activation = 'softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam')

''' This section is used for training '''

# define the checkpoint
filepath = "weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
callbacks_list = [checkpoint]

# fit model to the dataset
model.fit(X, Y, nb_epoch = 50, batch_size = 64, callbacks = callbacks_list)

''' This section is used to perform the text generation '''

# load the network weights
#filename = "weights-improvement-09-6.5041.hdf5"
#model.load_weights(filename)
#model.compile(loss='categorical_crossentropy', optimizer='adam')

# pick a random seed
#start = numpy.random.randint(0, len(dataX)-1)
#pattern = dataX[start]
#print "Seed:"
#print "\"", ' '.join([int_to_word[value] for value in pattern]), "\""

#for i in range(100):
#	x = numpy.reshape(pattern, (1, len(pattern), 1))
#	x = x / float(n_vocab)
#	prediction = model.predict(x, verbose = 0)
#	index = numpy.argmax(prediction)
#	result = int_to_word[index]
#	sys.stdout.write(result + " ")
#	pattern.append(index)
#	pattern = pattern[1:len(pattern)]

print "\nDone."
